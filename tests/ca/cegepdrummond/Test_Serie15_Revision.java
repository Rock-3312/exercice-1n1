package ca.cegepdrummond;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Test_Serie15_Revision extends SimulConsole{
    @Test
    @Order(1)
    void test_revision1() throws Exception {
        choixMenu("15a");
        ecrire("allo");
        ecrire("olla");
        assertSortie("anagramme", false);
        
        choixMenu(".");
        ecrire("manoir");
        ecrire("romain");
        assertSortie("anagramme", false);

        choixMenu(".");
        ecrire("argent");
        ecrire("garent");
        assertSortie("anagramme", false);

        choixMenu(".");
        ecrire("allo");
        ecrire("toto");
        assertSortie("pas un anagramme", false);
    }

    @Test
    @Order(2)
    void test_revision2() throws Exception {
        choixMenu("15b");
        ecrire("5 6 4 7 2 1");
        assertSortie("1 2 4 6 7 ", false);

        choixMenu(".");
        ecrire("5 5 4 3 2 1");
        assertSortie("1 2 3 4 5 ", false);

        choixMenu(".");
        ecrire("5 1 2 3 4 5");
        assertSortie("1 2 3 4 5 ", false);

        choixMenu(".");
        ecrire("6 6 5 4 3 2 1");
        assertSortie("1 2 3 4 5 6 ", false);
    }

}
